﻿using System;
using System.Collections.Generic;
using UnityEngine;
using XML;
using Targets;
using UI;
using Config;

namespace Game
{
    [Serializable]
    public struct MenuElements
    {
        public Sprite menuIcon;
        public Color color;
        public int score;
    }

    public class GameController : MonoBehaviour
    {
        private int TotalScore
        {
            get
            {
                return this._totalScore;
            }

            set
            {
                this._totalScore = value;
                this.ScoreUpdateEvent(this._totalScore);
                PlayerPrefs.SetInt(GameConfig.scoreField, this._totalScore);
            }
        }

        public Action<List<MenuElements>> MenuUpdateEvent;
        public Action<int> ScoreUpdateEvent;
        public Action WowAppearEvent;
        public UIController uiController;

        private int _totalScore;
        private List<GameObject> _targetPrefabsList = new List<GameObject>();
        private List<MenuElements> _menuElementsList = new List<MenuElements>();

        private void Start()
        {
            this.CreateTargetsMenu(0);
            this.TotalScore = PlayerPrefs.GetInt(GameConfig.scoreField);
            this.uiController.OnSpawnPrimitiveCommandEvent += SpawnPrimitive;
            this.uiController.OnChangeXMLEvent += CreateTargetsMenu;
        }

        private void OnDestroy()
        {
            this.uiController.OnSpawnPrimitiveCommandEvent -= SpawnPrimitive;
            this.uiController.OnChangeXMLEvent -= CreateTargetsMenu;
        }

        private void CreateTargetsMenu(int xmlNumber)
        {
            string path = string.Empty;

            switch (xmlNumber)
            {
                case 0:
                    {
                        path = GameConfig.XMLFile0;
                        break;
                    }

                case 1:
                    {
                        path = GameConfig.XMLFile1;
                        break;
                    }

                case 2:
                    {
                        path = GameConfig.XMLFile2;
                        break;
                    }
            }

            Dictionary<string, string> targetsDictionary = XMLParcer.LoadXMLfile(path);
            this._targetPrefabsList.Clear();
            this._menuElementsList.Clear();

            foreach (KeyValuePair<string, string> entry in targetsDictionary)
            {
                GameObject primitivePrefab = Resources.Load(entry.Key) as GameObject;
                this._targetPrefabsList.Add(primitivePrefab);
                PrimitiveController primitiveController = primitivePrefab.GetComponent<PrimitiveController>();

                MenuElements menuElements = new MenuElements();
                menuElements.menuIcon = primitiveController.menuIcon;
                menuElements.color = primitiveController.color;
                menuElements.score = Convert.ToInt32(entry.Value);
                this._menuElementsList.Add(menuElements);
            }

            this.MenuUpdateEvent(this._menuElementsList);
        }

        private void SpawnPrimitive(int prefabNumber)
        {
            float randomX = UnityEngine.Random.Range(GameConfig.generationZoneMinX, GameConfig.generationZoneMaxX);
            float randomY = UnityEngine.Random.Range(GameConfig.generationZoneMinY, GameConfig.generationZoneMaxY);
            float randomZ = UnityEngine.Random.Range(GameConfig.generationZoneMinZ, GameConfig.generationZoneMaxZ);
            GameObject target = Instantiate(this._targetPrefabsList[prefabNumber], new Vector3(randomX, randomY, randomZ), Quaternion.identity) as GameObject;
            PrimitiveController primitiveController = target.GetComponent<PrimitiveController>();
            primitiveController.score = this._menuElementsList[prefabNumber].score;
            primitiveController.OnDestroyScoreEvent += UpdateScore;
            primitiveController.OnDestroyAnimationEvent += InvokeAnimationEvent;
        }

        private void UpdateScore(int score)
        {
            this.TotalScore += score;
        }

        private void InvokeAnimationEvent()
        {
            this.WowAppearEvent();
        }
    }
}
