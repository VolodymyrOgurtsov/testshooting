﻿namespace Config
{
    public class GameConfig
    {
        public static string XMLFile0 = "targets0";
        public static string XMLFile1 = "targets1";
        public static string XMLFile2 = "targets2";
        public static string scoreField = "score";
        public static string nameField = "name";
        public static string wowTriggerName = "WowTrigger";
        public static float waitTime = 1.0f;
        public static float generationZoneMinX = -2.0f;
        public static float generationZoneMaxX = 2.0f;
        public static float generationZoneMinY = 0.5f;
        public static float generationZoneMaxY = 0.5f;
        public static float generationZoneMinZ = -2.0f;
        public static float generationZoneMaxZ = 2.0f;
    }
}
