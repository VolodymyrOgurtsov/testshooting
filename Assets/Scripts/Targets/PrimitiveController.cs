﻿using Config;
using System;
using System.Collections;
using UnityEngine;

namespace Targets
{
    public class PrimitiveController : MonoBehaviour
    {
        public Color color;
        public Sprite menuIcon;
        [HideInInspector]
        public int score;
        public Action<int> OnDestroyScoreEvent;
        public Action OnDestroyAnimationEvent;

        void Awake()
        {
            this.GetComponent<MeshRenderer>().material.color = this.color;
        }

        public virtual void OnMouseDown()
        {
            this.OnDestroyScoreEvent(this.score);
            this.OnDestroyScoreEvent = null;
            this.OnDestroyAnimationEvent = null;
            this.GetComponent<AudioSource>().Play();
            this.GetComponent<Collider>().enabled = false;
            this.GetComponent<MeshRenderer>().enabled = false;
            StartCoroutine(WaitBeforeDestroy(GameConfig.waitTime));
        }

        private IEnumerator WaitBeforeDestroy(float waitTime)
        {
            yield return new WaitForSeconds(waitTime);
            Destroy(this.gameObject);
        }
    }
}
