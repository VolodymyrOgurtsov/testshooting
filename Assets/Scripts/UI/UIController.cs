﻿using Config;
using Game;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class UIController : MonoBehaviour
    {
        public GameController gameController;
        public Transform contentPanel;
        public GameObject targetMenuPanelPrefab;
        public Text scoreText;
        public Action<int> OnSpawnPrimitiveCommandEvent;
        public Action<int> OnChangeXMLEvent;

        private void Awake()
        {
            this.gameController.MenuUpdateEvent += UpdateMenu;
            this.gameController.ScoreUpdateEvent += UpdateScoreText;
            this.gameController.WowAppearEvent += WowAnimation;
        }

        private void OnDestroy()
        {
            this.gameController.MenuUpdateEvent -= UpdateMenu;
            this.gameController.ScoreUpdateEvent += UpdateScoreText;
            this.gameController.WowAppearEvent -= WowAnimation;
        }

        private void UpdateMenu(List<MenuElements> menuElementsList)
        {
            for (int i = 0; i < contentPanel.childCount; i++)
            {
                Destroy(contentPanel.GetChild(i).gameObject);
            }

            for (int i = 0; i < menuElementsList.Count; i++)
            {
                GameObject targetMenuPanel = Instantiate(this.targetMenuPanelPrefab, this.contentPanel) as GameObject;
                TargetMenu targetMenu = targetMenuPanel.GetComponent<TargetMenu>();
                targetMenu.FillMenuPanel(i, menuElementsList[i]);
                targetMenu.OnSpawnButtonClickEvent += OnSpawnButtonClick;
            }
        }

        private void UpdateScoreText(int score)
        {
            scoreText.text = score.ToString();
        }

        private void OnSpawnButtonClick(int panelNumber)
        {
            this.OnSpawnPrimitiveCommandEvent(panelNumber);
        }

        public void OnXMLButtonClick(int buttonNumber)
        {
            this.OnChangeXMLEvent(buttonNumber);
        }

        public void WowAnimation()
        {
            this.GetComponent<Animator>().SetTrigger(GameConfig.wowTriggerName);
        }
    }
}
