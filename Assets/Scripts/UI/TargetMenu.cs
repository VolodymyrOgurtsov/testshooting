﻿using Game;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class TargetMenu : MonoBehaviour
    {
        public Image iconImage;
        public Text scoreText;
        public Action<int> OnSpawnButtonClickEvent;

        private int _panelNumber;

        public void FillMenuPanel(int panelNumber, MenuElements menuElements)
        {
            this._panelNumber = panelNumber;
            this.iconImage.sprite = menuElements.menuIcon;
            this.iconImage.color = menuElements.color;
            this.scoreText.text = menuElements.score.ToString();
        }

        public void OnSpawnButtonClick()
        {
            this.OnSpawnButtonClickEvent(this._panelNumber);
        }

        private void OnDestroy()
        {
            this.OnSpawnButtonClickEvent = null;
        }
    }
}
