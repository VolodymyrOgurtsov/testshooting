﻿using Config;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

namespace XML
{
    public class XMLParcer
    {
        public static Dictionary<string, string> LoadXMLfile(string path)
        {
            TextAsset textAsset = (TextAsset)Resources.Load(path);
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(textAsset.text);

            XmlElement element = xml.DocumentElement;
            IEnumerator elemEnum = element.GetEnumerator();

            Dictionary<string, string> targetsDictionary = new Dictionary<string, string>();

            while (elemEnum.MoveNext())
            {
                XmlElement xmlItem = (XmlElement)elemEnum.Current;

                if (!targetsDictionary.ContainsKey(xmlItem.GetAttribute(GameConfig.nameField)))
                {
                    targetsDictionary.Add(xmlItem.GetAttribute(GameConfig.nameField), xmlItem.InnerText);
                }
            }

            return targetsDictionary;
        }
    }
}